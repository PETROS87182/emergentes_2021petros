import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListPublicationsComponent } from './list-publications/list-publications.component';
import { CreatePublicationsComponent } from './create-publications/create-publications.component';
import { SharedModule } from '@shared/shared.module';
import { PublicationsRoutingModule } from './publications-routing.module';
import { CreateGastronomiaComponent } from './create-gastronomia/create-gastronomia.component';
import { CreateEventosComponent } from './create-eventos/create-eventos.component';
import { CreatePerfilComponent } from './create-perfil/create-perfil.component';


@NgModule({
  declarations: [
    ListPublicationsComponent,
    CreatePublicationsComponent,
    CreateGastronomiaComponent,
    CreateEventosComponent,
    CreatePerfilComponent
  ],
  imports: [
    SharedModule,
    PublicationsRoutingModule
  ]
})
export class PublicationsModule { }
