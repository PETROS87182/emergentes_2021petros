import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreatePublicationsComponent } from './create-publications/create-publications.component';
import { ListPublicationsComponent } from './list-publications/list-publications.component';
import { CreateGastronomiaComponent } from './create-gastronomia/create-gastronomia.component';
import { CreateEventosComponent } from './create-eventos/create-eventos.component';
import { CreatePerfilComponent } from './create-perfil/create-perfil.component';




const routes: Routes = [
    {
        path:'create-publications',
        component:CreatePublicationsComponent
    },
    {
      path:'list-publications',
      component:ListPublicationsComponent
    },
    {
      path:'create-eventos',
      component:CreateGastronomiaComponent
    },
    {
      path:'create-gastronomia',
      component:CreateEventosComponent
    },
    {
      path:'create-perfil',
      component:CreatePerfilComponent
    },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PublicationsRoutingModule { }
