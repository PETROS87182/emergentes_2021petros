import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListPublicationsComponent } from './list-publications.component';



@NgModule({
  declarations: [
    ListPublicationsComponent
  ],
  imports: [
    CommonModule
  ]
})
export class ListPublicationsModule { }
