import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import * as fromComponents from './components'
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {HttpClientModule} from '@angular/common/http'



@NgModule({
  imports: [
    FormsModule,// para los formularios
    ReactiveFormsModule,// variables reactivas
    HttpClientModule,//para comunicarnos con el restapi ->backend
    CommonModule
  ],
declarations: [
  ...fromComponents.components
],
exports:[
    FormsModule,// para los formularios
    ReactiveFormsModule,// variables reactivas
    HttpClientModule,//para comunicarnos con el restapi ->backend
    CommonModule,
    ...fromComponents.components
]
})

export class SharedModule { }
