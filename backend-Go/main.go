package main

import (
	"log"
	"net/http"
	"restapi/database"
	routers "restapi/routes"
)

func main() {
	database.Connect()
	router := routers.Setup()
	server := http.ListenAndServe(":3300", router)
	log.Fatal(server)
}
