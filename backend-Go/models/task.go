package models

import (
	"time"
)

type Task struct {
	Text     string    `bson:"text`
	createAt time.Time `bson:"created_at"`
	UpdateAt time.Time `bson:"updated_at"`
}
